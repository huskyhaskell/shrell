use std::io::{self, Write, Read};
use std::ffi::{CString};
use std::process;

use nix::unistd::{self, fork, ForkResult};
use nix::sys::wait::{self, WaitStatus, WaitPidFlag};

const SHRELL_RL_BUFSIZE: usize = 1024;
const SHRELL_TOK_BUFSIZE: usize = 64;
const EXIT_FAILURE: i32 = 1;
const EXIT_SUCCESS: i32 = 0;
const SHRELL_BUILTIN_STR: [&'static str; 3] = ["cd", "help", "exit"];
const SHRELL_BUILTIN_FUNCS: [fn(&Vec<CString>) -> Status; 3] = [shrell_cd, shrell_help, shrell_exit];

enum Status {
    Exit,
    Continue,
}

fn main() {
    shrell_loop();
}

fn shrell_loop() {
    let mut line: String;
    let mut args: Vec<CString>;
    let mut status: Status;

    loop {
	print!("> ");
	io::stdout().flush();
	line = shrell_read_line();
	args = shrell_split_line(&line);
	if let Status::Exit = shrell_execute(&args) { break; }
    }
}

fn shrell_read_line() -> String {
    let mut buffer: String = String::with_capacity(SHRELL_RL_BUFSIZE);

    while let Some(Ok(ch)) = std::io::stdin()
	.bytes()
	.next() {
	    if ch == '\n' as u8 {
		break;
	    } else {
		buffer.push(ch as char);
	    }
	}

    buffer
}

fn shrell_split_line(line: &String) -> Vec<CString> {
    let mut tokens: Vec<CString> = Vec::with_capacity(SHRELL_TOK_BUFSIZE);

    for tok in line.as_str().split_whitespace() {
	match CString::new(tok) {
	    Ok(tok_as_cstr) => tokens.push(tok_as_cstr),
	    Err(_) => continue,
	}
    }	

    tokens
}

fn shrell_launch(args: &Vec<CString>) -> Status {
    match unsafe { fork() } {
	Ok(ForkResult::Child) => {
	    if let Err(err) =
		unistd::execvp(&args[0], args) {
		eprintln!("shrell: {}", err);
	    }
	    process::exit(EXIT_FAILURE);
	},
	Ok(ForkResult::Parent { child, .. }) => {
	    loop {
		match wait::waitpid(child, Some(WaitPidFlag::WUNTRACED)) {
		    Ok(WaitStatus::Exited(_, _)) | Ok(WaitStatus::Signaled(_, _, _)) => break,
		    _ => {},
		}
	    }
	},
	Err(err) => eprintln!("shrell: {}", err),
    }

    Status::Continue
}

fn shrell_cd(args: &Vec<CString>) -> Status {
    if args.len() == 1 {
	eprintln!("shrell: expected argument to \"cd\"");
    } else {
	match unistd::chdir(args[1].as_c_str()) {
	    Err(err) => eprintln!("shrell: {}", err),
	    _ => {},
	}
    }

    Status::Continue
}

fn shrell_help(args: &Vec<CString>) -> Status {
    println!("David Holmqvist's shrell");
    println!("Type program names and arguments, and hit enter.");
    println!("The following are built in:");

    for builtin in &SHRELL_BUILTIN_STR {
	print!("  {:?}", builtin);
    }

    println!("\nUse the man command for information on other programs.");

    Status::Continue
}

fn shrell_exit(args: &Vec<CString>) -> Status {
    Status::Exit
}

fn find_builtin(cmd: &CString) -> Option<fn(&Vec<CString>) -> Status> {
    match cmd.to_str() {
	Ok(cmd) => {
	    for (idx, builtin) in SHRELL_BUILTIN_STR.iter().enumerate() {
		if &cmd == builtin {
		    return Some(SHRELL_BUILTIN_FUNCS[idx]);
		}
	    }
	    None
	},
	Err(_) => None,
    }
}

fn shrell_execute(args: &Vec<CString>) -> Status {
    if args.len() == 0 {
	Status::Continue
    } else if let Some(cmd) = find_builtin(&args[0]) {
	cmd(args)
    } else {
	shrell_launch(args)
    }
}
