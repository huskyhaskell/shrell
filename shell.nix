{ pkgs ? import <nixpkgs> {}
}:

pkgs.mkShell {
  name="dev-environment";
  buildInputs = with pkgs; [
    rustc
    cargo
    gdb
  ];
  shellHook = ''
    echo "Starting Rust dev..."
  '';
}
